'use strict';

/**
 * @fileoverview
 * Proxy server for front-end development.
 */

//------------------------------------------------------------------------------
//
//  Modules
//
//------------------------------------------------------------------------------

var dotenv = require('dotenv');

var os = require('os');
var util = require('util');
var path = require('path');
var fs = require('fs');
var childProcess = require('child_process');

var mkdirp = require('mkdirp');
var watch = require('node-watch');

var autoprefixer = require('autoprefixer');
var postcss = require('postcss');

var http = require('http');
var https = require('https');
var express = require('express');
var useragent = require('express-useragent');
var basicAuth = require('basic-auth-connect');

var httpProxy = require('http-proxy');
var harmon = require('harmon');
var livereload = require('livereload');
var connectLivereload = require('connect-livereload');

var debug = require('debug')('front-proxy');

//------------------------------------------------------------------------------
//
//  Constants
//
//------------------------------------------------------------------------------

dotenv.config({silent: true});

//--------------------------------------
//  Server
//--------------------------------------

var HOST = process.env.HOST || '0.0.0.0';

var PORT = process.env.PORT || 8080;

//--------------------------------------
//  LiveReload
//--------------------------------------

var LIVERELOAD_PORT = process.env.LIVERELOAD_PORT || 35729;

//--------------------------------------
//  Proxy
//--------------------------------------

var USE_PROXY;

if (process.env.USE_PROXY) {
  USE_PROXY = process.env.USE_PROXY == 'true';
} else {
  USE_PROXY = false;
}

var PROXY_TARGET = process.env.PROXY_TARGET;

var PROXY_COOKIE = process.env.PROXY_COOKIE;

//--------------------------------------
//  SSL Key & Cert
//--------------------------------------

var USE_SSL;

if (process.env.USE_SSL) {
  USE_SSL = process.env.USE_SSL == 'true';
} else {
  USE_SSL = false;
}

var SSL_KEY_PATH = process.env.SSL_KEY_PATH;

var SSL_CERT_PATH = process.env.SSL_CERT_PATH;

var ENABLE_SSL_CERT_VERIFICATION = process.env.ENABLE_SSL_CERT_VERIFICATION || false;

//--------------------------------------
//  Basic Auth
//--------------------------------------

var BASIC_AUTH_USERNAME = process.env.BASIC_AUTH_USERNAME;

var BASIC_AUTH_PASSWORD = process.env.BASIC_AUTH_PASSWORD;

//--------------------------------------
//  Response HTTP status
//--------------------------------------

var HTTP_STATUS_OK = 200;

var HTTP_STATUS_BAD_REQUEST = 400;

var HTTP_STATUS_SERVER_ERROR = 500;

//------------------------------------------------------------------------------
//
//  Variables
//
//------------------------------------------------------------------------------

var pid = process.pid;

var hostname = os.hostname();

var app;

var proxyServer;

var livereloadServer;

//------------------------------------------------------------------------------
//
//  Initialize
//
//------------------------------------------------------------------------------

app = express();

//--------------------------------------
//  Basic Auth
//--------------------------------------

debug('basic auth: (%s/%s)', BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD);

if (BASIC_AUTH_USERNAME && BASIC_AUTH_PASSWORD) {
  app.use(basicAuth(BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD));
}

//--------------------------------------
//  Sass Watch
//--------------------------------------

mkdirp('.tmp');

app.use(express.static('.tmp'));

// TODO: set sass source path
// childProcess.spawn('sh', ['-c',
//   [
//     'node_modules/node-sass/bin/node-sass src/styles/ --output .tmp/styles/',
//     '--source-map true',
//     '--include-path bower_components/bourbon/app/assets/stylesheets',
//     '--include-path bower_components/neat/app/assets/stylesheets',
//   ].join(' '),
// ], {
//   stdio: 'inherit',
// });

// TODO: set sass source path
// childProcess.spawn('sh', ['-c',
//   [
//     'node_modules/node-sass/bin/node-sass src/styles/ --output .tmp/styles/',
//     '--watch --recursive',
//     '--source-map true',
//     '--include-path bower_components/bourbon/app/assets/stylesheets',
//     '--include-path bower_components/neat/app/assets/stylesheets',
//   ].join(' '),
// ], {
//   stdio: 'inherit',
// });

watch('./.tmp/styles', {}, function(filename) {
  debug('watch sass output: %s', filename);

  if (!filename.match(/\.css$/) || filename.match(/\-post\.css$/)) {
    return;
  }

  var cssFrom = filename;
  var cssTo = filename.replace(/\.css$/, '-post.css');

  debug('postcss(autoprefixer) target: %s', cssFrom);

  postcss([autoprefixer({ browsers: [
    'Android >= 4',
    'last 1 ChromeAndroid versions',
    'iOS >= 7',
    'last 1 Chrome versions',
    'last 1 Safari versions',
    'last 1 Firefox versions',
    'Explorer >= 9',
  ] })])
  .process(fs.readFileSync(cssFrom), {
    from: cssFrom,
    to: cssTo,
    map: { inline: false },
  }).then(
    function(result) {
      fs.writeFileSync(cssTo, result.css);

      if (result.map) {
        fs.writeFileSync(cssTo + '.map', result.map);
      }
    },

    function(error) {
      console.log('postcss(autoprefixer) error:', error);
    }
  );

});

//--------------------------------------
//  Webpack Watch
//--------------------------------------

// Not Recommend using Webfuck
// childProcess.spawn('sh', ['-c',
//   'node node_modules/webpack/bin/webpack.js --watch --config webpack.config.js',
// ], {
//   stdio: 'inherit',
// });

// Replace WebPack output. (e.g. drop `webpackMissingModule` errors)
// watch('../static/build', {}, function(filename) {
//   debug('watch webpack output: %s', filename);
//
//   if (!filename.match(/\.js$/)) {
//     return;
//   }
//
//   fs.readFile(filename, 'utf8', function(err, data) {
//     if (err) throw err;
//
//     filename = filename.replace('../static', '.tmp/static');
//     debug('transformed webpack output: %s', filename);
//
//     data = data.replace(/.*webpackMissingModule.*/g, '');
//
//     mkdirp(path.dirname(filename), function(err) {
//       if (err) throw err;
//       fs.writeFile(filename, data, 'utf8');
//     });
//   });
// });

//--------------------------------------
//  HTML Transform
//--------------------------------------

if (USE_PROXY) {
  var selects = [];

  // Replace HTML contents (e.g. replace css path)
  // selects.push({
  //   query: 'link',
  //   func: function(node) {
  //     try {
  //       var stream = node.createStream({ outer: true });
  //
  //       var tag = '';
  //
  //       stream.on('data', function(data) {
  //         tag += data;
  //       });
  //
  //       stream.on('end', function() {
  //         var srcMatch = tag.match(/href="(.+)"/);
  //
  //         if (srcMatch && srcMatch.length > 1) {
  //           debug('link href: %s', srcMatch[1] + '\n');
  //         }
  //
  //         tag = tag.replace(/\/styles\/before\/(.+)\.css/, '/styles/after/$1.css');
  //
  //         stream.end(tag);
  //       });
  //     } catch (error) {
  //       console.error('harmon error:', error);
  //     }
  //   },
  // });

  app.use(harmon([], selects, true));
}

//--------------------------------------
//  LiveReload
//--------------------------------------

var liveReloadWatchPaths = ['.tmp', 'views'];

var liveReloadOption = {
  port: LIVERELOAD_PORT,
  applyJSLive: true,
  applyCSSLive: true,
};

if (USE_SSL) {
  liveReloadOption.https = {
    key: fs.readFileSync(SSL_KEY_PATH, 'utf8'),
    cert: fs.readFileSync(SSL_CERT_PATH, 'utf8'),
  };
}

debug(liveReloadOption);
livereloadServer = livereload.createServer(liveReloadOption);

// livereloadServer.watch(liveReloadWatchPaths);

console.log('front proxy livereload watch: [%s]', liveReloadWatchPaths);

app.use(connectLivereload());

//--------------------------------------
//  Proxy Target
//--------------------------------------

if (USE_PROXY) {
  proxyServer = httpProxy.createProxyServer({});

  proxyServer.on('proxyReq', function(proxyReq, req, res) {
    debug('http-proxy %s %s request', req.method, req.path);

    if (PROXY_COOKIE) {
      proxyReq.setHeader('Cookie', PROXY_COOKIE);
    }
  });

  proxyServer.on('proxyRes', function(proxyRes, req, res) {
    debug('http-proxy %s %s response: %s', req.method, req.path, res.statusCode);
  });

  proxyServer.on('error', function(err, req, res) {
    console.error('error on http-proxy:', err);
    res.status(HTTP_STATUS_SERVER_ERROR).end();
  });

  proxyServer.on('open', function(proxySocket) {
    debug('http-proxy websocket open');

    proxySocket.on('data', function(data) {
      debug('http-proxy websocket data:', data);
    });
  });

  proxyServer.on('close', function(req, socket, head) {
    debug('http-proxy websocket close');
  });

  function proxyRoute(req, res) {
    proxyServer.web(req, res, {
      // If proxy target protocol is https
      ssl: {
        key: fs.readFileSync(SSL_KEY_PATH, 'utf8'),
        cert: fs.readFileSync(SSL_CERT_PATH, 'utf8'),
      },
      target: PROXY_TARGET,
      secure: ENABLE_SSL_CERT_VERIFICATION,
    });
  }

  console.log('front proxy target: [%s]', PROXY_TARGET);
  app.use(proxyRoute);
}

//--------------------------------------
//  Routing
//--------------------------------------

app.use(express.static(__dirname + '/public'));
app.use(useragent.express());

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

if (!USE_PROXY) {
  app.get('/', function(req, res) {
    res.render('default', {
      device: req.useragent.isMobile ? 'mobile' : 'desktop',
      view: 'index',
    });
  });

  app.get('/api(/:name)?', function(req, res) {
    try {
      var filePath = path.join(__dirname, 'data/' + req.params.name + '.json');

      if (fs.existsSync(filePath)) {
        var file = fs.readFileSync(filePath);
        res.json(JSON.parse(file));
      } else {
        res.status(HTTP_STATUS_BAD_REQUEST).end();
      }
    } catch (error) {
      console.error(error);
      res.status(HTTP_STATUS_SERVER_ERROR).end();
    }
  });

  app.get('/generated-image(/:name)?', function(req, res) {
    try {
      var filePath = path.join(__dirname, 'assets/images/' + req.params.name);

      if (fs.existsSync(filePath)) {
        res.sendFile(filePath);
      } else {
        res.status(HTTP_STATUS_BAD_REQUEST).end();
      }
    } catch (error) {
      console.error(error);
      res.status(HTTP_STATUS_SERVER_ERROR).end();
    }
  });

  app.get('/:view', function(req, res) {
    res.render('default', {
      device: req.useragent.isMobile ? 'mobile' : 'desktop',
      view: req.params.view,
    });
  });
}

//--------------------------------------
//  Main Server
//--------------------------------------

var serverCallback = function(error) {
  if (error) {
    console.error('front proxy server listen error:', error);
    process.exit(1);
  }

  console.log('front proxy server started: %s@%s (%s:%s)', pid, hostname, HOST, PORT);
};

if (USE_SSL) {
  // If proxy target protocol is https
  https.createServer({
    key: fs.readFileSync(SSL_KEY_PATH, 'utf8'),
    cert: fs.readFileSync(SSL_CERT_PATH, 'utf8'),
  }, app).listen(PORT, HOST, serverCallback);
} else {
  http.createServer(app).listen(PORT, HOST, serverCallback);
}
