# Front Proxy

Proxy server for front-end development.

## Usage

```sh
npm start
```

## Options

Create a `.env` file in the root directory of your project.

```sh
# proxy, ssl options
USE_PROXY="true|false" # default false
USE_SSL="true|false" # default false

# proxy options
PROXY_TARGET="<proxy-target-url>"
PROXY_COOKIE="<proxy-cookie>" # e.g. cookie for login status

# ssl key & cert
SSL_KEY_PATH="<ssl-key-file-path>"
SSL_CERT_PATH="<ssl-cert-file-path>"
ENABLE_SSL_CERT_VERIFICATION="true|false" # default false

# server options
HOST="<proxy-server-host>" # default 0.0.0.0
PORT="<proxy-server-port>" # default 8080
LIVERELOAD_PORT="<livereload-server-port>" # default 35729

# basic auth
BASIC_AUTH_USERNAME="<basic-auth-username>"
BASIC_AUTH_PASSWORD="<basic-auth-password>"
```